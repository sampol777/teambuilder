package com.example.teamchooser.model.remote

interface TeamsService {
    suspend fun getTeams(): MutableList<Pair<String,Int>>
    suspend fun addPair(newPair: Pair<String,Int>): MutableList<Pair<String,Int>>
}