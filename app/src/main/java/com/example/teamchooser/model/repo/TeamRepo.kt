package com.example.teamchooser.model.repo

import com.example.teamchooser.model.remote.TeamsService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


object TeamRepo {


    suspend fun addPair(pair: Pair<String, Int>) = withContext(Dispatchers.IO){
       return@withContext teamsService.addPair(pair)
    }

    suspend fun getTeams(): MutableList<Pair<String, Int>> = withContext(Dispatchers.IO) {
        delay(3000)
        return@withContext teamsService.getTeams()
    }
}


val teamsService: TeamsService = object : TeamsService {
    private val myList: MutableList<Pair<String, Int>> = mutableListOf()
    override suspend fun getTeams(): MutableList<Pair<String, Int>> = myList
    override suspend fun addPair(newPair: Pair<String, Int>): MutableList<Pair<String, Int>> {
        return myList.also {
            it.add(newPair)
        }
    }


}