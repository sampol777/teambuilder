package com.example.teamchooser.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.teamchooser.databinding.FragmentTeamsPageBinding
import com.example.teamchooser.view.adapters.TeamsAdapter
import com.example.teamchooser.viewmodel.TeamsViewModel

class TeamsPageFragment : Fragment() {
    private val teamVM by viewModels<TeamsViewModel>()
    private val teams1Adapter: TeamsAdapter = TeamsAdapter(mutableListOf())
    private val teams2Adapter: TeamsAdapter = TeamsAdapter(mutableListOf())
    lateinit var binding: FragmentTeamsPageBinding

    var TAG = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTeamsPageBinding.inflate(inflater, container, false)
        initViews()
        initObservers()
        return binding.root
    }

    fun initObservers() {
        teamVM.currentTeamsList.observe(viewLifecycleOwner) { teamState ->
            teams1Adapter.updateList(teamState.teamList1)
            teams2Adapter.updateList(teamState.teamList2)
        }
    }

    private fun initViews() {
        with(binding) {
            team1.layoutManager =
                GridLayoutManager(this@TeamsPageFragment.context, 1)
            team1.adapter = teams1Adapter

            team2.layoutManager =
                GridLayoutManager(this@TeamsPageFragment.context, 1)
            team2.adapter = teams2Adapter

            Log.e(TAG, "initViews: $")



            back.setOnClickListener {
                val action = TeamsPageFragmentDirections.actionFragmentTeamsPageToMainPageFragment()
                findNavController().navigate(action)
            }
        }
    }


}