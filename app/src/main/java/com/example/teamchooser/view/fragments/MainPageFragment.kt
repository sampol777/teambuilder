package com.example.teamchooser.view.fragments

import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.teamchooser.databinding.FragmentMainPageBinding
import com.example.teamchooser.model.repo.TeamRepo
import com.example.teamchooser.viewmodel.TeamsViewModel

class MainPageFragment: Fragment() {
    lateinit var binding:FragmentMainPageBinding
    private val teamViewModel by viewModels<TeamsViewModel>()
    lateinit var teamsState:RecyclerState
    var TAG = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainPageBinding.inflate(inflater,container, false)
        initObservers()
        initViews()
        return binding.root
    }

    fun initViews(){
        binding.btnTeam1.setOnClickListener(){
            val name = binding.nameInput.text.toString()
            teamViewModel.addPair(name,1)
            Log.e(TAG, "initViews: $teamsState", )



        }
        binding.btnTeam2.setOnClickListener(){
            val name = binding.nameInput.text.toString()
            teamViewModel.addPair(name,2)
            Log.e(TAG, "initViews: $teamsState", )
        }
        binding.btnTeams.setOnClickListener(){
            val action = MainPageFragmentDirections.actionMainPageFragmentToFragmentTeamsPage()
            findNavController().navigate(action)
        }
    }

    fun initObservers(){
        with(binding){
            teamViewModel.currentTeamsList.observe(viewLifecycleOwner){ tState ->
                teamsState = tState

            }
        }
    }
}