package com.example.teamchooser.view.fragments

data class RecyclerState(
    var isLoading:Boolean = false,
    var teamList1:List<String> = listOf(),
    var teamList2:List<String> = listOf()


)
