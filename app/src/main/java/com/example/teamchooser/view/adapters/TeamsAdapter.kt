package com.example.teamchooser.view.adapters

import android.content.ContentValues.TAG
import android.nfc.Tag
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.teamchooser.databinding.CardLayoutBinding

class TeamsAdapter(
    private val teams: MutableList<String>
) : RecyclerView.Adapter<TeamsAdapter.TeamsViewHolder>() {

    class TeamsViewHolder(
        private val binding:CardLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            Log.d(TAG, "Creating TeamsViewHolder")
        }
        fun loadName(name: String){
            binding.itemName.text = name
        }
    }

    fun updateList(newList: List<String>){
        val oldSize = teams.size
        teams.clear()
        notifyItemRangeChanged(0,oldSize)
        teams.addAll(newList)
        notifyItemRangeChanged(0,newList.size)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TeamsAdapter.TeamsViewHolder {
       val binding: CardLayoutBinding = CardLayoutBinding.inflate(
           LayoutInflater.from(parent.context),
           parent,
           false
       )
        return TeamsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TeamsAdapter.TeamsViewHolder, position: Int) {
        val name = teams[position]
        holder.loadName(name)
    }

    override fun getItemCount(): Int {
       return teams.size
    }
}
