package com.example.teamchooser.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamchooser.model.repo.TeamRepo
import com.example.teamchooser.view.fragments.RecyclerState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TeamsViewModel : ViewModel() {

    private var _currentTeamsList: MutableLiveData<RecyclerState> = MutableLiveData(RecyclerState())
    val currentTeamsList: LiveData<RecyclerState> get() = _currentTeamsList

    init {
        viewModelScope.launch {
            separateTeams(
                getTeams()
            )
        }
    }

    fun addPair(string: String, int: Int) = viewModelScope.launch {
        val team = TeamRepo.addPair(Pair(string, int))
        separateTeams(team)
    }

    fun separateTeams(list: List<Pair<String, Int>>) {
        val team1 = mutableListOf<String>()
        val team2 = mutableListOf<String>()

        list.forEach {
            if (it.second == 1) team1.add(it.first) else team2.add(it.first)
        }

        _currentTeamsList.value = _currentTeamsList.value?.copy(
            teamList1 = team1,
            teamList2 = team2
        )
    }


    private suspend fun getTeams() = withContext(Dispatchers.IO) {
        return@withContext TeamRepo.getTeams()

    }


}